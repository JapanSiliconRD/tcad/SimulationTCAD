################################################################
#  < Device file >
#  Perform IV scan
#  2016/07/29 Created by Koji Nakamura<Koji.Nakamura@cern.ch>
################################################################

File {
	grid    = "@tdr@"	
	Output	= "@log@"        
        plot    = "@tdrdat@"
        current = "@plot@"
}

Electrode {
{name = "TopAl"	  voltage = 0.0}
#if [string compare @Type@ "MOS1"] == 1
  {name = "NPlus"	  voltage = 0.0}
#endif
#if [string compare @Type@ "MOS4"] == 1
  {name = "PStop"    voltage = 0.0}
#endif
{name = "BackSideP"    voltage = 0.0}
}

Physics {
  Mobility(      DopingDep
  	         HighFieldSaturation
  	         Enormal
  	         CarrierCarrierScattering
 	          )
  Recombination( SRH(DopingDep)
  		 Auger
                 Avalanche
                  )

  EffectiveIntrinsicDensity( OldSlotboom )
}



Plot {
        Current/Vector
	eCurrent/Vector
	hCurrent
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	DopingConcentration
	SpaceCharge
	srhRecombination
	AugerRecombination
	AvalancheGeneration
	TotalRecombination
	eMobility
	hMobility	
}	
	

Math {  
	
		
	Method=Pardiso
	Number_of_threads = 4
	Stacksize = 200000000		
	Extrapolate
	Derivatives
	AvalDerivatives
	RelErrControl
	Iterations=20
        Notdamped=100
        
        BreakCriteria {
            
#if [string compare @Type@ "MOS1"] == 1
            Current (Contact = "NPlus" maxval = 1e-5)
#endif
#if [string compare @Type@ "MOS4"] == 1
            Current (Contact = "PStop" maxval = 1e-5)
#endif
            Current (Contact = "BackSideP"  maxval = 1e-5)
            Current (Contact = "TopAl"  maxval = 1e-5)
        }
              
        NoAutomaticCircuitContact

}

Solve {

    Coupled {Poisson}
    Coupled {Hole Poisson}
    Coupled {Electron Hole Poisson} 

      QuasiStationary (
    	InitialStep = 1e-2
	MaxStep = 1
	MinStep = 1e-8
	Goal { Name= "BackSideP" Voltage=@Vop@}


	Plot { Range=(0 1) Intervals=1 }
    ) { Coupled { Hole Electron Poisson Circuit} }
}





