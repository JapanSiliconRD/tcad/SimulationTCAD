
################################################################
#  < Device file >
#  This device file is making IV curves
#  Created 2016/06/20 by hamasaki ryutaro (SOKENDAI)
#  Modified 2017/04/04 by Koji Nakamura
################################################################

File {
	grid    = "@tdr@"
	Output	= "@log@"
        plot    = "@tdrdat@"
#        current = "@plot@"
        current = "IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_des.plt"
}

Electrode {

{name = "ElectrodeN1"	  voltage = 0.0}
{name = "ElectrodeN2"	  voltage = 0.0}
{name = "ElectrodeN3"	  voltage = 0.0}
{name = "ElectrodeP"      voltage = 0.0}
{name = "BiasRail"        voltage = 0.0}


}


Physics {


  Mobility(      DopingDep
  	         HighFieldSaturation
  	         Enormal
  	         CarrierCarrierScattering
 	          )

  Recombination( SRH(DopingDep)
  		 Auger
                 Avalanche
                  )

  EffectiveIntrinsicDensity( OldSlotboom )

  Traps (FixedCharge Conc=@surfCharge@)
}




Plot {
        Current/Vector
	eCurrent/Vector
	hCurrent
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	DopingConcentration
	SpaceCharge
	srhRecombination
	AugerRecombination
	AvalancheGeneration
	TotalRecombination
	eMobility
	hMobility
}


Math {
	Method=Pardiso
	Number_of_threads = 4
	Stacksize = 200000000
	Extrapolate
	Derivatives
	AvalDerivatives
	RelErrControl
	Iterations=20
        Notdamped=100

        BreakCriteria {

            Current (Contact = "ElectrodeN1" maxval = 1e-5)
            Current (Contact = "ElectrodeN2" maxval = 1e-5)
            Current (Contact = "ElectrodeN3" maxval = 1e-5)
            Current (Contact = "ElectrodeP"  maxval = 1e-5)
        }

        NoAutomaticCircuitContact

}

Solve {

    Coupled {Poisson}
    Coupled {Hole Poisson}
    Coupled {Electron Hole Poisson}

      QuasiStationary (
    	InitialStep = 1e-2
	MaxStep = 1
	MinStep = 1e-8
#    	InitialStep = 1
#	MaxStep = 10
#	MinStep = 1e-6
	Goal { Name= "ElectrodeP" Voltage=@Vop@}


	Plot { Range=(0 1) Intervals=1 }
    ) { Coupled { Hole Electron Poisson Circuit} }
#   Save (FilePrefix = "IV_@Vop@_@tPSub@_@Fluence@")
   Save (FilePrefix = "IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@")
}




