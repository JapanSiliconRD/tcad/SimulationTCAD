# TCAD package for Silicon R&D
##  Author Koji Nakamura <Koji.Nakamura@cern.ch>
####   first created on 4th Apr 2017


## [Enviroment Setup]
### need License to use TCAD
####  ask relevant people to get TCAD Sentaurus License
### setup 
    export STROOT=/afs/cern.ch/eng/clic/software/Sentaurus
    export PATH=$STROOT/bin:$PATH
    export LM_LICENSE_FILE=XXXXXXXXXXXXXXXX
    export STDB=/Path/To/Your/Work/Dir 

## [Checkout package]

    git clone https://:@gitlab.cern.ch:8443/kojin/SimulationTCAD.git SimulationTCAD
    cd SimulationTCAD

## [Package]
### current pacakges
    % find * -type d
    2D
    2D/MOS
    2D/MOS/MOScap
    2D/Pixel
    2D/Pixel/PixelBREffi



