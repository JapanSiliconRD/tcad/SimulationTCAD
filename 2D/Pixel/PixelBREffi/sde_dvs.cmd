

;*****************************************************************************************************
; < Structure file >
;
; This example structure file make a microstrip sensor of 1+0.5*2 N+ sensor and 2 Pstop with P- substrate
; Created 2016/06/20 by Koji Nakamura (KEK)
; Modified 2016/08/26 by hiromi sawai (TokyoTech)
; Modified 2017/04/04 by Koji Nakamura (KEK)
; In order to check
;
;     -  IV curve and transient MIP analysis
;     -  Radiation hardness (fix charge dependece)
;
;
;   x=Xmin=0                                 x=Xmax
;              Poly-Si
;    _______   _   ______________      _______   y = -tAl
;   |Al_____|_|_|_|______Al______|____|_____Al|  y = -tOx
;   ||       SiO2        ||       SiO2       ||
;   ||________   ________||_______   ________||  y = Ymin =0
;   |N+____|  |P|  |____N+____|   |P|  |____N+|
;   |                                         |
;   |                                         |
;   |                 Bulk                    |
;   |                 (p-Si)                  |
;   |_________________________________________|
;   |___________________P+____________________|  y = Ymax = tPSub
;
;
;   This figure shows X-Y structure and 2D device simulation.
;
;*****************************************************************************************************

(sde:clear)

;====================
;***** Variable *****
;====================


;***************
; X axis
;***************

(define Xmin 0)

;Sensor



(define PixSize	@wPixSize@    ) ; PixelSize  default 250
(define WNPlus	    @wNplus@)   ; With of Nplus   default 228
(define WPStop     @wPstop@)  	; With of PStop   default 5
(define WAl        @wAlum@ )    ; Width of Aluminum default 240
(define WOx        @wOxide@ )   ; Width of Oxide  default 240
(define WBr         @wBR@)    ; Width of Bias rail
(define factor     0.8) 			     ; lateral diffusion factor (0.8)


(define XMIP       @XMIP@)
(define Dpix (- (/ (- PixSize WNPlus) 2) (/ WPStop 2)));Distance of n+ edge to p-stop edge
(define Dal   (- PixSize WAl)) ;Distance between aluminium
(define Dox   (- PixSize WOx)) ;Distance between SiO2
(define Dbr   (/ (- Dal WBr) 2)) ;Distance between bias rail and aluminum


;******************
; Y axis
;******************

(define Ymin	0)

;Sensor

(define tPSub       @tPSub@)  ; Thickness substrate
(define tNPlus      0.5)   ; Diffusion Length of Nplus
(define tPStop      1.2) ; Diffusion Length of PStop
(define tPPlus      2) ; Diffusion Length of Back side of PPlus
(define tOx         0.2)
(define tOxBr       @tOxBr@)
(define Ymax (+ Ymin tPSub))  ; Total length in Y axis

(define tAl        1)
(define tBr        1)


;******************
; Dose concentration
;******************

(cond 
 ((string=? "@Fluence@" "NonIrrad") (define Dop_PSub    1e+12))
 ((string=? "@Fluence@" "Irrad3e15") (define Dop_PSub    4e+13))
 (else (define Dop_PSub     @dop_PSub@))
); nonirrad 1e+12  irrad 4e+13 (3x10^15 neq/cm2)

(define Dop_NPlus    1e+19) 	;
(define Dop_PStop    1e+16) 	;
(define Dop_PPlus    1e+19) 	;

;***************************************************************************************

;===============================
;***** Diffusion parameters *****
;===============================


;*** Nplus1: diffusion NPlus ****

(define NPlus1_Min Xmin)
(define NPlus1_Max (/ WNPlus 2))

(define NPlus1_Peak Ymin)
(define NPlus1_Junction (+ Ymin tNPlus))

;*** Pstop1: diffusion PPlus ****

(define PStop1_Min (+ NPlus1_Max Dpix))
(define PStop1_Max (+ PStop1_Min WPStop))

(define PStop1_Peak Ymin)
(define PStop1_Junction (+ Ymin tPStop))

;*** Nplus2: diffusion NPlus ****

(define NPlus2_Min (+ PStop1_Max Dpix))
(define NPlus2_Max (+ NPlus2_Min WNPlus))

(define NPlus2_Peak Ymin)
(define NPlus2_Junction (+ Ymin tNPlus))

;*** Pstop2: diffusion PPlus ****

(define PStop2_Min (+ NPlus2_Max Dpix))
(define PStop2_Max (+ PStop2_Min WPStop))

(define PStop2_Peak Ymin)
(define PStop2_Junction (+ Ymin tPStop))

;*** Nplus3: diffusion NPlus ****

(define NPlus3_Min (+ PStop2_Max Dpix))
(define NPlus3_Max (+ NPlus3_Min (/ WNPlus 2)))

(define NPlus3_Peak Ymin)
(define NPlus3_Junction (+ Ymin tNPlus))

(define Xmax NPlus3_Max)


;*** Back side Difusion Electrodoe P (Pplus) ****

(define XElectrodeP_Min Xmin)
(define XElectrodeP_Max Xmax)

(define YElectrodeP_Peak 0)
(define YElectrodeP_Junction tPPlus)

;***  Ox1 ****

(define XOx1_Min (/ Dox 2))
(define XOx1_Max (+ XOx1_Min WOx))
(define YOx1_Min 0)
(define YOx1_Max (- YOx1_Min tOx))

;***  Ox2 ****

(define XOx2_Min (+ XOx1_Max Dox))
(define XOx2_Max (+ XOx2_Min WOx))
(define YOx2_Min 0)
(define YOx2_Max (- YOx1_Min tOx))

;***  Ox3 ****
;***  Al1 ****

(define XAl1_Min Xmin)
(define XAl1_Max (/ WAl 2))
(define YAl1_Min YOx1_Max)
(define YAl1_Max (- YAl1_Min tAl))

;***  Al2 ****

(define XAl2_Min (+ XAl1_Max Dal))
(define XAl2_Max (+ XAl2_Min WAl))
(define YAl2_Min YOx1_Max)
(define YAl2_Max (- YAl1_Min tAl))

;***  Al3 ****

(define XAl3_Min (+ XAl2_Max Dal))
(define XAl3_Max (+ XAl3_Min (/ WAl 2)))
(define YAl3_Min YOx1_Max)
(define YAl3_Max (- YAl1_Min tAl))

;***  Bias ***

(define XBiasOX_Min (+ XAl1_Max Dbr))
(define XBiasOX_Max (+ XBiasOX_Min WBr))
(define YBiasOX_Min YOx1_Max)
(define YBiasOX_Max (- YBiasOX_Min tOxBr))

(define XBias_Min (+ XAl1_Max Dbr))
(define XBias_Max (+ XBias_Min WBr))
(define YBias_Min YBiasOX_Max)
(define YBias_Max (- YBias_Min tBr))


;*** Contact Electrode N+ (NCont) ****

(define XNCont1_Min NPlus1_Min)
(define XNCont1_Max NPlus1_Max)

;*** Contact Electrode N+ (NCont) ****

(define XNCont2_Min NPlus2_Min)
(define XNCont2_Max NPlus2_Max)

;*** Contact Electrode N+ (NCont) ****

(define XNCont3_Min NPlus3_Min)
(define XNCont3_Max NPlus3_Max)


;*** Contact Electrode P+ (PCont) ****

(define XPCont_Min  XElectrodeP_Min)
(define XPCont_Max  XElectrodeP_Max)

;*** Contact Bias rail (BiasCont) ****

(define XBiasCont_Min   XBias_Min)
(define XBiasCont_Max   XBias_Max)


;***************************************************************************************


;=================================================
;**** Drawing structures (Si,SiO2,Electrode) ****
;=================================================

;-------------------------------------------------
(sdegeo:set-default-boolean "ABA")
;-------------------------------------------------

;===============================
;*** Substrate creation (SUB) ***
;===============================

(sdegeo:create-rectangle (position Xmin Ymin 0) (position Xmax Ymax 0) "Silicon" "SUB")
(sdegeo:create-rectangle (position XOx1_Min YOx1_Min 0) (position XOx1_Max YOx1_Max 0) "Oxide" "Ox1")
(sdegeo:create-rectangle (position XOx2_Min YOx2_Min 0) (position XOx2_Max YOx2_Max 0) "Oxide" "Ox2")
(if (> tOxBr 0) (sdegeo:create-rectangle (position XBiasOX_Min YBiasOX_Min 0) (position XBiasOX_Max YBiasOX_Max 0) "Oxide" "Ox3"))

;===============================
;*** Aluminum creation (ALUMI) ***
;===============================

;(sdegeo:create-rectangle (position XAl1_Min YAl1_Min 0) (position XAl1_Max YAl1_Max 0) "Aluminum" "ALUMI1")
(sdegeo:create-polygon 
 (list
  (position XAl1_Min YAl1_Max 0) 
  (position XAl1_Max YAl1_Max 0) 
  (position XAl1_Max YAl1_Min 0)
  (position XOx1_Min YAl1_Min 0) 
  (position XOx1_Min YOx1_Min 0) 
  (position XAl1_Min YOx1_Min 0)
  (position XAl1_Min YAl1_Max 0) ) 
 "Aluminum" "ALUMI1")
;(sdegeo:create-rectangle (position XAl2_Min YAl2_Min 0) (position XAl2_Max YAl2_Max 0) "Aluminum" "ALUMI2")
(sdegeo:create-polygon 
 (list
  (position XAl2_Min YAl2_Max 0) 
  (position XAl2_Max YAl2_Max 0) 
  (position XAl2_Max YAl2_Min 0)
  (position XOx2_Min YAl2_Min 0) 
  (position XOx2_Min YOx1_Min 0) 
  (position XOx1_Max YOx1_Min 0)
  (position XOx1_Max YAl2_Min 0)
  (position XAl2_Min YAl2_Min 0)
  (position XAl2_Min YAl2_Max 0) ) 
 "Aluminum" "ALUMI2")
;(sdegeo:create-rectangle (position XAl3_Min YAl3_Min 0) (position XAl3_Max YAl3_Max 0) "Aluminum" "ALUMI3")
(sdegeo:create-polygon 
 (list
  (position XAl3_Min YAl3_Max 0) 
  (position XAl3_Max YAl3_Max 0) 
  (position XAl3_Max YOx2_Min 0)
  (position XOx2_Max YOx2_Min 0) 
  (position XOx2_Max YAl3_Min 0) 
  (position XAl3_Min YAl3_Min 0)
  (position XAl3_Min YAl3_Max 0) ) 
 "Aluminum" "ALUMI3")

;===============================
;*** Bias Rail creation (BIAS) ***
;===============================

(sdegeo:create-rectangle (position XBias_Min YBias_Min 0) (position XBias_Max YBias_Max 0) "PolySilicon" "BIAS")


;===============================
;*** Contact definitions ***
;===============================

(sdegeo:define-contact-set "ElectrodeN1" 4.0 (color:rgb 1.0 0.0 0.0) "##")
(sdegeo:define-contact-set "ElectrodeN2" 4.0 (color:rgb 1.0 0.0 0.0) "##")
(sdegeo:define-contact-set "ElectrodeN3" 4.0 (color:rgb 1.0 0.0 0.0) "##")
(sdegeo:define-contact-set "ElectrodeP" 4.0 (color:rgb 0.0 0.0 1.0) "##")
(sdegeo:define-contact-set "BiasRail" 4.0 (color:rgb 0.0 1.0 0.0) "##")

;*** Electrode N1 ****

(sdegeo:insert-vertex (position XNCont1_Min Ymin 0))
(sdegeo:insert-vertex (position XNCont1_Max Ymin 0))

;(define Xn1 (+ XNCont1_Min (/ WNPlus 2)))
(define Xn1 (+ XNCont1_Min (/ WNPlus 4)))

(sdegeo:set-current-contact-set "ElectrodeN1")
(define CN1 (find-edge-id (position Xn1 Ymin 0)))
(sdegeo:set-contact-edges CN1)

;*** Electrode N2 ****

(sdegeo:insert-vertex (position XNCont2_Min Ymin 0))
(sdegeo:insert-vertex (position XNCont2_Max Ymin 0))

(define Xn2 (+ XNCont2_Min (/ WNPlus 2)))

(sdegeo:set-current-contact-set "ElectrodeN2")
(define CN2 (find-edge-id (position Xn2 Ymin 0)))
(sdegeo:set-contact-edges CN2)

;*** Electrode N3 ****

(sdegeo:insert-vertex (position XNCont3_Min Ymin 0))
(sdegeo:insert-vertex (position XNCont3_Max Ymin 0))

;(define Xn3 (+ XNCont3_Min (/ WNPlus 2)))
(define Xn3 (+ XNCont3_Min (/ WNPlus 4)))

(sdegeo:set-current-contact-set "ElectrodeN3")
(define CN3 (find-edge-id (position Xn3 Ymin 0)))
(sdegeo:set-contact-edges CN3)

;*** Electrode P    ****

(sdegeo:insert-vertex (position XPCont_Min Ymax 0))
(sdegeo:insert-vertex (position XPCont_Max Ymax 0))

(define XP (+ XPCont_Min (/ Xmax 2)))

(sdegeo:set-current-contact-set "ElectrodeP")
(define CP (find-edge-id (position XP Ymax 0)))
(sdegeo:set-contact-edges CP)

;*** Bias rail    ****

(sdegeo:insert-vertex (position XBiasCont_Min Ymax 0))
(sdegeo:insert-vertex (position XBiasCont_Max Ymax 0))

(define XB (+ XBiasCont_Min (/ WBr 2)))

(sdegeo:set-current-contact-set "BiasRail")
(define CB (find-edge-id (position XB YBias_Max 0)))
(sdegeo:set-contact-edges CB)


;===============================
;DOPING REGIONS DEFINITIONS
;===============================

;*** Substrate: Doping constant ****

(sdepe:doping-constant-placement "Dop_PSub" "BoronActiveConcentration" Dop_PSub "SUB")

;*** Diffusion of Nplus1  ****

(sdedr:define-refeval-window "NPlus1" "Line" (position NPlus1_Min Ymin 0) (position NPlus1_Max Ymin 0))
(sdedr:define-gaussian-profile "NPlus1" "PhosphorusActiveConcentration" "PeakPos" NPlus1_Peak "PeakVal" Dop_NPlus "ValueAtDepth" Dop_PSub "Depth" NPlus1_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "NPlus1" "NPlus1" "NPlus1" "NoSymm" "NoReplace" "Eval")

;*** Diffusion of PStop1  ****

(sdedr:define-refeval-window "PStop1" "Line" (position PStop1_Min Ymin  0) (position PStop1_Max Ymin 0))
(sdedr:define-gaussian-profile "PStop1" "BoronActiveConcentration" "PeakPos" PStop1_Peak "PeakVal" Dop_PStop "ValueAtDepth" Dop_PSub "Depth" PStop1_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "PStop1" "PStop1" "PStop1" "NoSymm" "NoReplace" "Eval")

;*** Diffusion of Nplus2  ****

(sdedr:define-refeval-window "NPlus2" "Line" (position NPlus2_Min Ymin 0) (position NPlus2_Max Ymin 0))
(sdedr:define-gaussian-profile "NPlus2" "PhosphorusActiveConcentration" "PeakPos" NPlus2_Peak "PeakVal" Dop_NPlus "ValueAtDepth" Dop_PSub "Depth" NPlus2_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "NPlus2" "NPlus2" "NPlus2" "NoSymm" "NoReplace" "Eval")

;*** Diffusion of PStop2  ****

(sdedr:define-refeval-window "PStop2" "Line" (position PStop2_Min Ymin  0) (position PStop2_Max Ymin 0))
(sdedr:define-gaussian-profile "PStop2" "BoronActiveConcentration" "PeakPos" PStop2_Peak "PeakVal" Dop_PStop "ValueAtDepth" Dop_PSub "Depth" PStop2_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "PStop2" "PStop2" "PStop2" "NoSymm" "NoReplace" "Eval")

;*** Diffusion of Nplus3  ****

(sdedr:define-refeval-window "NPlus3" "Line" (position NPlus3_Min Ymin 0) (position NPlus3_Max Ymin 0))
(sdedr:define-gaussian-profile "NPlus3" "PhosphorusActiveConcentration" "PeakPos" NPlus3_Peak "PeakVal" Dop_NPlus "ValueAtDepth" Dop_PSub "Depth" NPlus3_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "NPlus3" "NPlus3" "NPlus3" "NoSymm" "NoReplace" "Eval")

;*** Difusion Electrode P

(sdedr:define-refeval-window "PPlus"  "Line" (position XElectrodeP_Min Ymax 0) (position XElectrodeP_Max Ymax 0))
(sdedr:define-gaussian-profile "PPlus" "BoronActiveConcentration" "PeakPos" YElectrodeP_Peak "PeakVal" Dop_PPlus "ValueAtDepth" Dop_PSub "Depth" YElectrodeP_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "PPlus" "PPlus" "PPlus" "Negative" "NoReplace" "Eval")


;--------------------------------------------------------------------------
;Meshing Strategy

(define DXmesh (* 8 factor))
(define DXmeshO (* 18 factor))
(define DXmesh1 (* 10 factor))
(define DXmesh3 (* 6 factor))
(define DXmesh4 (* 14 factor))
(define DXmesh5 (* 3 factor))

;Refinament ALL(Xmax Ymax Xmin Ymin)

(sdedr:define-refeval-window "RFW1" "Rectangle" (position Xmin Ymin 0) (position Xmax Ymax 0))
(sdedr:define-refinement-size "RFS1" (/ Xmax 50) (/ Ymax 10) (/ Xmax 500) (/ Ymax 100))
(sdedr:define-refinement-placement "RFP1" "RFS1" "RFW1")

;Refinament NPlus1

(sdedr:define-refeval-window "RFW_ElectrodeN1" "Rectangle" (position (- NPlus1_Min DXmesh1) Ymin 0) (position (+ NPlus1_Max DXmesh1) (+ tNPlus DXmesh1) 0))
(sdedr:define-refinement-size "RFS_ElectrodeN1" (/ WNPlus 30) (/ tNPlus 2) (/ WNPlus 1000) (/ tNPlus 50)) ;might have to change due to half size
(sdedr:define-refinement-function "RFS_ElectrodeN1" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_ElectrodeN1" "RFS_ElectrodeN1" "RFW_ElectrodeN1")


;Refinament NPlus2

(sdedr:define-refeval-window "RFW_ElectrodeN2" "Rectangle" (position (- NPlus2_Min DXmesh1) Ymin 0) (position (+ NPlus2_Max DXmesh1) (+ tNPlus DXmesh1) 0))
(sdedr:define-refinement-size "RFS_ElectrodeN2" (/ WNPlus 30) (/ tNPlus 2) (/ WNPlus 1000) (/ tNPlus 50))
(sdedr:define-refinement-function "RFS_ElectrodeN2" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_ElectrodeN2" "RFS_ElectrodeN2" "RFW_ElectrodeN2")


;Refinament NPlus3

(sdedr:define-refeval-window "RFW_ElectrodeN3" "Rectangle" (position (- NPlus3_Min DXmesh1) Ymin 0) (position (+ NPlus3_Max DXmesh1) (+ tNPlus DXmesh1) 0))
(sdedr:define-refinement-size "RFS_ElectrodeN3" (/ WNPlus 30) (/ tNPlus 2) (/ WNPlus 1000) (/ tNPlus 50)) ;might have to change due to half size
(sdedr:define-refinement-function "RFS_ElectrodeN3" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_ElectrodeN3" "RFS_ElectrodeN3" "RFW_ElectrodeN3")


;Refinament PPlus Anode

(sdedr:define-refeval-window "RFW_ElectrodeP" "Rectangle" (position XElectrodeP_Min Ymax 0) (position XElectrodeP_Max (- (- Ymax tPPlus) DXmesh1) 0))
(sdedr:define-refinement-size "RFS_ElectrodeP" (/ Xmax 50) (/ tPPlus 2) (/ Xmax 1000) (/ tPPlus 50))
(sdedr:define-refinement-function "RFS_ElectrodeP" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_ElectrodeP" "RFS_ElectrodeP" "RFW_ElectrodeP")

;HERE!!!!!!!!!!!!!!!!!!

; Si/SiO2 interface


(sdedr:define-refeval-window "RFW_Ch1" "Rectangle" (position (+ XOx1_Min DXmesh5) (+ Ymin 0.1) 0) (position (- XOx1_Max DXmesh5) Ymin 0))
(sdedr:define-multibox-size "RFS_Ch1" 100 0.1 10 0.01  1.5 2)
(sdedr:define-multibox-placement "RFP_Ch1" "RFS_Ch1" "RFW_Ch1" )

(sdedr:define-refeval-window "RFW_Ch2" "Rectangle" (position (+ XOx1_Min DXmesh5) Ymin 0) (position (- XOx1_Max DXmesh5) (- Ymin 0.1) 0))
(sdedr:define-multibox-size "RFS_Ch2" 100 0.1 10 0.01  1.5 -2)
(sdedr:define-multibox-placement "RFP_Ch2" "RFS_Ch2" "RFW_Ch2" )

(sdedr:define-refeval-window "RFW_Ch3" "Rectangle" (position  (+ XOx2_Min DXmesh5) (+ Ymin 0.1) 0) (position  (- XOx2_Max DXmesh5) Ymin 0))
(sdedr:define-multibox-size "RFS_Ch3" 100 0.1 10 0.01  1.5 2)
(sdedr:define-multibox-placement "RFP_Ch3" "RFS_Ch3" "RFW_Ch3" )

(sdedr:define-refeval-window "RFW_Ch4" "Rectangle" (position (+ XOx2_Min DXmesh5) Ymin 0) (position (- XOx2_Max DXmesh5) (- Ymin 0.1) 0))
(sdedr:define-multibox-size "RFS_Ch4" 100 0.1 10 0.01  1.5 -2)
(sdedr:define-multibox-placement "RFP_Ch4" "RFS_Ch4" "RFW_Ch4" )




; MIP at XMIP

(if (> XMIP 0)
  (begin 
    (display "XMIP is not 0")
    (sdedr:define-refeval-window "RFW_MIP0" "Rectangle" (position (- XMIP DXmesh5) Ymin 0) (position XMIP Ymax 0))
    (sdedr:define-multibox-size "RFS_MIP0" 1 10 0.005 10  -2 1)
    (sdedr:define-multibox-placement "RFP_MIP0" "RFS_MIP0" "RFW_MIP0" )
    (sdedr:define-refeval-window "RFW_MIP1" "Rectangle" (position XMIP Ymin 0) (position (+ XMIP DXmesh5) Ymax 0))
    (sdedr:define-multibox-size "RFS_MIP1" 1 10 0.005 10  2 1)
    (sdedr:define-multibox-placement "RFP_MIP1" "RFS_MIP1" "RFW_MIP1" )
  )
  (begin 
    (display "XMIP is 0")
  )
)


; Saving BND file--------------------------------------------------------

(sdeio:save-tdr-bnd (get-body-list) "@tdrboundary/o@")

;Saving CMD file

(sdedr:write-cmd-file "@commands/o@")

;Build Mesh
(sde:build-mesh "snmesh" " " "n@node@_msh")
