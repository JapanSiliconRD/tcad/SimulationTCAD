################################################################
#  < Device file >
#  This device file is making MIP simulation
#  Created 2016/06/20 by hamasaki ryutaro (SOKENDAI)
#  Modified 2017/04/04 by Koji Nakamura
################################################################


Electrode {

	{name = "ElectrodeN1"	  voltage = 0.0}
	{name = "ElectrodeN2"	  voltage = 0.0}	
	{name = "ElectrodeN3"	  voltage = 0.0}	
	{name = "ElectrodeP"      voltage = 0.0}
	{name = "BiasRail"        voltage = 0.0}
}

File {
	Grid    =   "@tdr@"
	Parameter = "NewSRHRecombination.par" 
#	Current =   "MIP_des_@XMIP@.plt"  
	Current =   "_des.plt"  
       	plot    =   "MIP_des_@XMIP@.tdr"
       	Output  =   "MIP_des_@XMIP@.log"
       	
}


Physics {
		

  
  	Mobility(      DopingDep
  	    	       HighFieldSaturation
  	    	       Enormal
  	    	       CarrierCarrierScattering
 	  	        )

  	Recombination( SRH(DopingDep)
  		 	Auger
                 	      Avalanche
                        	)

    	EffectiveIntrinsicDensity( OldSlotboom )
    	
    	HeavyIon (
    		Direction=(0, 1)
    		Location=(@XMIP@, 0.0)
    		Time= 0.01e-9
#    		Length= [0.0 0.01 149.99 150]
    		Length= [0.0 0.01 @<@tPSub@-0.01>@ @tPSub@]
    		wt_hi= [1.0 1.0 1.0 1.0]
    		LET_f= [1.28e-5 1.28e-5 1.28e-5 1.28e-5]
    		Gaussian
    		Picocoulumb)
        Traps (FixedCharge Conc=@surfCharge@)
}



Plot {
       	hCurrent/Vector
	eCurrent/Vector
	hCurrent
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	Doping
	DonorConcentration
	AcceptorConcentration
	SpaceCharge
	srhRecombination
	AugerRecombination
	AvalancheGeneration
	TotalRecombination
	eMobility
	hMobility
	HeavyIonChargeDensity
	HeavyIonGeneration	
}	
	

Math {  
	
	Digits=5
	Iterations=1000
	Method=Pardiso
	Number_of_threads = 4
	Stacksize = 200000000		
	Extrapolate
	Derivatives
	RelErrControl
	Avalderivatives	        
        RecBoxIntegr(1e-4 100 10000)           
        
}



  	
Solve {
         
#    Load(FilePrefix="IV_-100_@tPSub@_@XMIP@")
#    Load(FilePrefix="IV_@Vop@_@tPSub@_@Fluence@")
    Load(FilePrefix="IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@")

#    NewCurrentPrefix="transient_IV_@Vop@_@tPSub@_@Fluence@_@XMIP@"
    NewCurrentPrefix="transient_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@"

   Transient (
	InitialTime = 0.0
	FinalTime = 0.1e-9
	InitialStep = 1e-14
	MaxStep = 2e-13
	MinStep = 1e-16
	Increment =1.2
	) 
	{ Coupled (iterations=8, notdamped=15) {Poisson Electron Hole}
	  Plot (FilePrefix="MIP_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_0_" Time= (0.005e-9; 0.01e-9; 0.02e-9; 0.03e-9; 0.04e-9; 0.1e-9) NoOverwrite )
	}
    
    Transient (
	InitialTime = 0.1e-9
	FinalTime = 1.5e-9
	InitialStep = 1e-13
	MaxStep = 1e-11
	MinStep = 1e-15
	Increment =1.1
	) 
	{ Coupled (iterations=8, notdamped=15) {Poisson Electron Hole}
	  Plot (FilePrefix="MIP_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_1_" Time= (0.11e-9; 0.13e-9; 0.15e-19; 0.17e-9; 0.19e-19; 0.2e-9; 0.4e-9; 1e-9; 1.5e-9) NoOverwrite )
	}
	
    Transient (
	InitialTime = 1.5e-9
	FinalTime = 1.0e-8
	InitialStep = 1e-11
	MaxStep = 1e-10
	MinStep = 1e-15
	Increment =1.1
	) 
	{ Coupled (iterations=8, notdamped=15) {Poisson Electron Hole}
	  Plot (FilePrefix="MIP_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_2_" Time= (1.5e-9; 2e-9; 2.5e-9; 3e-9; 3.5e-9; 4e-9; 5e-9; 6e-9; 7e-9; 8e-9; 9e-9; 1e-8) NoOverwrite )
	}
	
    Transient (
	InitialTime = 1.0e-8
	FinalTime = 5.0e-8
	InitialStep = 1e-11
	MaxStep = 1e-10
	MinStep = 1e-15
	Increment =1.1
	) 
	{ Coupled (iterations=8, notdamped=15) {Poisson Electron Hole}
	  Plot (FilePrefix="MIP_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_3_" Time= (1.5e-8; 2e-9; 2.5e-8; 3e-8; 3.5e-8; 4e-8; 5e-8) NoOverwrite )
	}   
	
   
	
}

