################################################################
#  < Device file >
#  Perform CV scan
#  2016/07/29 Created by Koji Nakamura<Koji.Nakamura@cern.ch>
################################################################


Device LGAD {
	File {
		grid    = "@tdr@"
        	plot    = "CV_@fHz@.tdr"
        	current = "CV_@fHz@.plt"
	}
       
	Electrode {
	{name = "TopAl"	  voltage = -20.0}
        {name = "NPlus"	  voltage = 0.0}
#        {name = "PStop"    voltage = 0.0}
#	{name = "BackSideP"	   voltage = 0.0}

	}

	Physics{
  	Mobility( DopingDep
  	    	HighFieldSaturation
  	    	Enormal
  	    	CarrierCarrierScattering
 	  	)
  	Recombination( SRH(DopingDep)
  		 	Auger
                 	Avalanche
                 	)

    	EffectiveIntrinsicDensity( OldSlotboom )
	}
}

	Plot {
        	Current/Vector
		eCurrent/Vector
		hCurrent
		eDensity
		hDensity
		ElectricField/Vector
		Potential
		DopingConcentration
		SpaceCharge
		srhRecombination
		AugerRecombination
		AvalancheGeneration
		TotalRecombination
		eMobility
		hMobility	
	}
	
	

System{
#       LGAD diodesystem ("NPlus"=0 "BackSideP"=back )
#       LGAD diodesystem ("PStop"=0 "BackSideP"=back )
       LGAD diodesystem ("TopAl"=0 "NPlus"=back )
       Vsource_pset vn (back 0) {dc=0}
}


File { 
	Output= "CV_@fHz@.log"
	ACExtract="CV_AC_@fHz@.plt"
}


Solve {

    Coupled (Iterations=50) {Poisson}
    Coupled (Iterations=15) {Hole Poisson}
    Coupled (Iterations=15) {Electron Hole Poisson} 
    Coupled (Iterations=15) {Electron Hole Poisson Contact}

    QuasiStationary (
	InitialStep = 1e-6
	MaxStep = 2e-2
	MinStep = 1e-12
	Increment = 2
	Decrement = 4
	Goal { Parameter=vn.dc Voltage = @Vdc@}
	) 
	{
  	ACCoupled (
   		StartFrequency=@fHz@
   		EndFrequency=@fHz@
   		NumberOfPoints=1 Decade
   		Iterations=15
   		Node (back)
   		ACMethod=Blocked
   		ACSubMethod("diodesystem")=Pardiso
   		) {Poisson Electron Hole Contact Circuit}
   	} 
}
