;*******************************************************************
; < Structure file > 
;
; Creating simple MOS structure to check CV curve before and after irradiation
; 2016/07/28 Created by Koji Nakamura<Koji.Nakamura@cern.ch>
; 
;
;  MOS1                      MOS3 		     MOS4
;  ============= Al          ============= Al        ============= Al	 
;  _____________ SiN(50)     _____________ SiN(0)    _____________ SiN(0)
;  _____________ SiO2(200)   _____________ SiO2(600) _____________ SiO2(300)
;   ----------   N+	        		      ----------   P+	 
;			   			    			 
;      p-bulk(320um)           p-bulk(320um)          p-bulk(320um)
;  ============= Al          ============= Al        ============= Al    
;
;
;*******************************************************************



(sde:clear)

;====================
;***** Variable *****
;====================


;***************
; X axis
;***************

(define Xmin 0)
(define Xmax 100)

;Sensor

(define WNPlus	    100)    ; With of Nplus
(define WPStop      100)    ; With of PStop
(define WTopAl      100)    ; With of TopAl
(define factor      0.8)     ; lateral diffusion factor (0.8)

(define XMIP       @XMIP@)
(define Dpix	     5) ;Distance of Each sensor 
(define Dside	     5) ;Distance of left side to Nplus 

;******************
; Y axis
;******************

(define Ymin	0)

;Sensor

(define tPSub       @tPSub@)  ; Thickness substrate
(define tNPlus      @tNPlus@)   ; Diffusion Length of Nplus (0.5 or 0)
(define tPStop      @tPStop@) ; Diffusion Length of PStop (1.2 or 0)
(define tPPlus      2) ; Diffusion Length of Back sie of PPlus
(define tSiO2       @tSiO2@)
(define tSiN        @tSiN@)
(define tTopAl      0.1)
(define Ymax (+ Ymin tPSub))  ; Total length in Y axis

;******************
; Dose concentration
;******************

(define Dop_PSub     1e+12) 	;
(define Dop_NPlus    1e+19) 	; 
(define Dop_PStop    1e+16) 	; 
(define Dop_PPlus    1e+19) 	; 

;********************

;===============================
;***** Diffusion parameters *****
;===============================


;*** Nplus: diffusion NPlus ****

(define NPlus_Min (+ Xmin Dside))
(define NPlus_Max (+ NPlus_Min WNPlus))

(define NPlus_Peak Ymin)
(define NPlus_Junction (+ Ymin tNPlus))

;*** Pstop: diffusion PPlus ****

(define PStop_Min (+ Xmin Dside))
(define PStop_Max (+ PStop_Min WPStop))

(define PStop_Peak Ymin)
(define PStop_Junction (+ Ymin tPStop))

;*** Back side Difusion BackSide P (Pplus) ****

(define XBackSideP_Min Xmin)
(define XBackSideP_Max Xmax)
(define YBackSideP_Peak 0)   ;;;;;;; Ymax            ????
(define YBackSideP_Junction tPPlus)  ;;;; ( - Ymax tPPlus ) ?????? 

;***  OxSiN ****

(define XSiN_Min Xmin)
(define XSiN_Max Xmax)
(define YSiN_Min 0)            
(define YSiN_Max (- YSiN_Min tSiN))

;***  OxSiO2 ****

(define XSiO2_Min Xmin)
(define XSiO2_Max Xmax)
(define YSiO2_Min YSiN_Max)            
(define YSiO2_Max (- YSiO2_Min tSiO2))

;***  TopAl ****
(define XTopAl_Min Xmin)
(define XTopAl_Max Xmax)
(define YTopAl_Min YSiO2_Max)            
(define YTopAl_Max (- YTopAl_Min tTopAl))


;*** Contact Electrode N+ (NCont) ****
(define XNPlusCont_Min NPlus_Min)
(define XNPlusCont_Max NPlus_Max)

;*** Contact Electrode N+ (NCont) ****
(define XPStopCont_Min PStop_Min)
(define XPStopCont_Max PStop_Max)

;*** Contact BackSide P+ (PCont) ****
(define XBackSidePCont_Min  XBackSideP_Min)
(define XBackSidePCont_Max  XBackSideP_Max)

;*** Contact Top Al (AlCont) ****
(define XTopAlCont_Min  XTopAl_Min)
(define XTopAlCont_Max  XTopAl_Max)

;=================================================
;**** Drawing structures (Si,SiO2,BackSide) ****
;=================================================

;-------------------------------------------------
(sdegeo:set-default-boolean "ABA")
;-------------------------------------------------

;===============================
;*** Substrate creation (SUB) ***
;===============================

(sdegeo:create-rectangle (position Xmin Ymin 0) (position Xmax Ymax 0) "Silicon" "SUB")
(if (string=? "@Type@" "MOS1")  (begin   (sdegeo:create-rectangle (position XSiN_Min YSiN_Min 0) (position XSiN_Max YSiN_Max 0) "Si3N4" "SiN")))
(sdegeo:create-rectangle (position XSiO2_Min YSiO2_Min 0) (position XSiO2_Max YSiO2_Max 0) "SiO2" "SiO2")
(define TopAlCont (sdegeo:create-rectangle (position XTopAl_Min YTopAl_Min 0) (position XTopAl_Max YTopAl_Max 0.0) "Aluminium" "TopAlumi"))

;===============================
;*** Contact definitions ***
;===============================

(if (string=? "@Type@" "MOS1")  (begin   (sdegeo:define-contact-set "NPlus" 4.0 (color:rgb 1.0 0.0 0.0) "##")))
(if (string=? "@Type@" "MOS4")  (begin   (sdegeo:define-contact-set "PStop" 4.0 (color:rgb 1.0 0.0 0.0) "##")))
(sdegeo:define-contact-set "BackSideP" 4.0 (color:rgb 0.0 0.0 1.0) "##")
(sdegeo:define-contact-set "TopAl" 4.0 (color:rgb 1.0 0.0 0.0) "##")




;*** Top Al ****
(sdegeo:set-current-contact-set "TopAl")
(sdegeo:set-contact-boundary-edges TopAlCont "TopAl" )
(sdegeo:delete-region TopAlCont)

;*** Electrode NPlus ****
(if (string=? "@Type@" "MOS1")  (begin   
  (sdegeo:insert-vertex (position XNPlusCont_Min Ymin 0))
  (sdegeo:insert-vertex (position XNPlusCont_Max Ymin 0))
  (define Xnplus (+ XNPlusCont_Min (/ WNPlus 2)))

  (sdegeo:set-current-contact-set "NPlus")
  (define CNplus (find-edge-id (position Xnplus Ymin 0)))
  (sdegeo:set-contact-edges CNplus)
))

;*** Electrode PStop ****
(if (string=? "@Type@" "MOS4")  (begin   
  (sdegeo:insert-vertex (position XPStopCont_Min Ymin 0))
  (sdegeo:insert-vertex (position XPStopCont_Max Ymin 0))
  (define Xpstop (+ XPStopCont_Min (/ WPStop 2)))

(sdegeo:set-current-contact-set "PStop")
(define CPstop (find-edge-id (position Xpstop Ymin 0)))
(sdegeo:set-contact-edges CPstop)
))

;*** BackSide P    ****

(sdegeo:insert-vertex (position XBackSidePCont_Min Ymax 0))
(sdegeo:insert-vertex (position XBackSidePCont_Max Ymax 0))

(define XP (+ XBackSidePCont_Min (/ Xmax 2)))

(sdegeo:set-current-contact-set "BackSideP")
(define CP (find-edge-id (position XP Ymax 0)))
(sdegeo:set-contact-edges CP)


;===============================
;DOPING REGIONS DEFINITIONS
;===============================

;*** Substrate: Doping constant ****

(sdepe:doping-constant-placement "Dop_PSub" "BoronActiveConcentration" Dop_PSub "SUB")

;*** Diffusion of Nplus  ****

(if (string=? "@Type@" "MOS1")  (begin   
(sdedr:define-refeval-window "NPlus" "Line" (position NPlus_Min Ymin 0) (position NPlus_Max Ymin 0))
(sdedr:define-gaussian-profile "NPlus" "PhosphorusActiveConcentration" "PeakPos" NPlus_Peak "PeakVal" Dop_NPlus "ValueAtDepth" Dop_PSub "Depth" NPlus_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "NPlus" "NPlus" "NPlus" "NoSymm" "NoReplace" "Eval")
))

;*** Diffusion of PStop  ****
(if (string=? "@Type@" "MOS4")  (begin   
  (sdedr:define-refeval-window "PStop" "Line" (position PStop_Min Ymin  0) (position PStop_Max Ymin 0))
  (sdedr:define-gaussian-profile "PStop" "BoronActiveConcentration" "PeakPos" PStop_Peak "PeakVal" Dop_PStop "ValueAtDepth" Dop_PSub "Depth" PStop_Junction "Gauss" "Factor" factor)
  (sdedr:define-analytical-profile-placement "PStop" "PStop" "PStop" "NoSymm" "NoReplace" "Eval")
))
;*** Difusion BackSide P

(sdedr:define-refeval-window "PPlus"  "Line" (position XBackSideP_Min Ymax 0) (position XBackSideP_Max Ymax 0))
(sdedr:define-gaussian-profile "PPlus" "BoronActiveConcentration" "PeakPos" YBackSideP_Peak "PeakVal" Dop_PPlus "ValueAtDepth" Dop_PSub "Depth" YBackSideP_Junction "Gauss" "Factor" factor)
(sdedr:define-analytical-profile-placement "PPlus" "PPlus" "PPlus" "Negative" "NoReplace" "Eval")

;--------------------------------------------------------------------------
;Meshing Strategy

(define DXmesh (* 8 factor))
(define DXmeshO (* 18 factor))
(define DXmesh1 (* 10 factor))
(define DXmesh3 (* 6 factor))
(define DXmesh4 (* 14 factor))
(define DXmesh5 (* 3 factor))

;Refinament ALL(Xmax Ymax Xmin Ymin)

(sdedr:define-refeval-window "RFW1" "Rectangle" (position Xmin Ymin 0) (position Xmax Ymax 0))
(sdedr:define-refinement-size "RFS1" (/ Xmax 50) (/ Ymax 10) (/ Xmax 500) (/ Ymax 100))
(sdedr:define-refinement-placement "RFP1" "RFS1" "RFW1")

;Refinament NPlus

(if (string=? "@Type@" "MOS1")  (begin   
(sdedr:define-refeval-window "RFW_NPlus" "Rectangle" (position (- NPlus_Min DXmesh1) Ymin 0) (position (+ NPlus_Max DXmesh1) (+ tNPlus DXmesh1) 0))
(sdedr:define-refinement-size "RFS_NPlus" (/ WNPlus 30) (/ tNPlus 2) (/ WNPlus 1000) (/ tNPlus 50))
(sdedr:define-refinement-function "RFS_NPlus" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_NPlus" "RFS_NPlus" "RFW_NPlus")
))

;Refinament PStop
(if (string=? "@Type@" "MOS4")  (begin   
  (sdedr:define-refeval-window "RFW_PStop" "Rectangle" (position (- PStop_Min DXmesh1) Ymin 0) (position (+ PStop_Max DXmesh1) (+ tPStop DXmesh1) 0))
  (sdedr:define-refinement-size "RFS_PStop" (/ WPStop 30) (/ tPStop 2) (/ WPStop 1000) (/ tPStop 50))
  (sdedr:define-refinement-function "RFS_PStop" "DopingConcentration" "MaxTransDiff" 0.4)
  (sdedr:define-refinement-placement "RFP_PStop" "RFS_PStop" "RFW_PStop")
))

;Refinament Backside PPlus Anode

(sdedr:define-refeval-window "RFW_BackSideP" "Rectangle" (position XBackSideP_Min Ymax 0) (position XBackSideP_Max (- (- Ymax tPPlus) DXmesh1) 0))
(sdedr:define-refinement-size "RFS_BackSideP" (/ Xmax 50) (/ tPPlus 2) (/ Xmax 1000) (/ tPPlus 50))
(sdedr:define-refinement-function "RFS_BackSideP" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_BackSideP" "RFS_BackSideP" "RFW_BackSideP")


; TopAl interface
(sdedr:define-refeval-window "RFW_TopAl" "Rectangle" (position (- XTopAl_Min DXmesh1) Ymin 0) (position (+ XTopAl_Max DXmesh1) (+ tTopAl DXmesh1) 0))
(sdedr:define-refinement-size "RFS_TopAl" (/ WTopAl 30) (/ tTopAl 2) (/ WTopAl 1000) (/ tTopAl 50))
(sdedr:define-refinement-function "RFS_TopAl" "DopingConcentration" "MaxTransDiff" 0.4)
(sdedr:define-refinement-placement "RFP_TopAl" "RFS_TopAl" "RFW_TopAl")


;(sdedr:define-refeval-window "RFW_TopAl" "Rectangle" (position (+ XTopAl_Min DXmesh5) (+ Ymin 0.1) 0) (position (- XTopAl_Max DXmesh5) Ymin 0))
;(sdedr:define-multibox-size "RFS_TopAl" 100 0.1 10 0.01  1.5 2) 
;(sdedr:define-multibox-placement "RFP_TopAl" "RFS_TopAl" "RFW_TopAl" )



; Si/SiO2 interface 

;;(if (string=? "@Type@" "MOS1")  (begin   
;;  (sdedr:define-refeval-window "RFW_Ch1" "Rectangle" (position (+ XSiN_Min DXmesh5) (+ Ymin 0.1) 0) (position (- XSiN_Max DXmesh5) Ymin 0))
;;  (sdedr:define-multibox-size "RFS_Ch1" 100 0.1 10 0.01  1.5 2) 
;;  (sdedr:define-multibox-placement "RFP_Ch1" "RFS_Ch1" "RFW_Ch1" )
;;
;;  (sdedr:define-refeval-window "RFW_Ch2" "Rectangle" (position (+ XSiN_Min DXmesh5) Ymin 0) (position (- XSiN_Max DXmesh5) (- Ymin 0.1) 0))
;;  (sdedr:define-multibox-size "RFS_Ch2" 100 0.1 10 0.01  1.5 -2) 
;;  (sdedr:define-multibox-placement "RFP_Ch2" "RFS_Ch2" "RFW_Ch2" )
;;))

(sdedr:define-refeval-window "RFW_Ch3" "Rectangle" (position  (+ XSiO2_Min DXmesh5) (+ Ymin 0.1) 0) (position  (- XSiO2_Max DXmesh5) Ymin 0))
(sdedr:define-multibox-size "RFS_Ch3" 100 0.1 10 0.01  1.5 2) 
(sdedr:define-multibox-placement "RFP_Ch3" "RFS_Ch3" "RFW_Ch3" )

(sdedr:define-refeval-window "RFW_Ch4" "Rectangle" (position (+ XSiO2_Min DXmesh5) Ymin 0) (position (- XSiO2_Max DXmesh5) (- Ymin 0.1) 0))
(sdedr:define-multibox-size "RFS_Ch4" 100 0.1 10 0.01  1.5 -2) 
(sdedr:define-multibox-placement "RFP_Ch4" "RFS_Ch4" "RFW_Ch4" )




; MIP at XMIP
;(sdedr:define-refeval-window "RFW_MIP0" "Rectangle" (position (- XMIP DXmesh5) Ymin 0) (position XMIP Ymax 0))
;(sdedr:define-multibox-size "RFS_MIP0" 1 10 0.005 10  -2 1) 
;(sdedr:define-multibox-placement "RFP_MIP0" "RFS_MIP0" "RFW_MIP0" )
;
;(sdedr:define-refeval-window "RFW_MIP1" "Rectangle" (position XMIP Ymin 0) (position (+ XMIP DXmesh5) Ymax 0))
;(sdedr:define-multibox-size "RFS_MIP1" 1 10 0.005 10  2 1) 
;(sdedr:define-multibox-placement "RFP_MIP1" "RFS_MIP1" "RFW_MIP1" )

; Saving BND file--------------------------------------------------------

(sdeio:save-tdr-bnd (get-body-list) "@tdrboundary/o@")

;Saving CMD file

(sdedr:write-cmd-file "@commands/o@")

;Build Mesh
(sde:build-mesh "snmesh" " " "n@node@_msh")

