load_library extend

#set projname transient_IV_@Vop@_@tPSub@_@Fluence@_@XMIP@MIP
set projname transient_IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@
proj_load ${projname}_des.plt $projname

#set N1 32
#proj_load n${N1}_des.plt curve1
proj_load IV_@Fluence@_sc@surfCharge@_@tPSub@um_Ox@tOxBr@_x@XMIP@_des.plt curve1
cv_createDS NO_NAME {curve1 ElectrodeP InnerVoltage} {curve1 ElectrodeN1 TotalCurrent} y
cv_createWithFormula curve_IV "<TotalCurrent_ElectrodeN1>" A A 
set LEAKAGE [cv_compute "vecvaly(<curve_IV>, @Vop@)" A A A A]

set c1 curve1
set c2 curve2
set c3 curve3
set c4 curve4

cv_createDS $c1 "$projname time" "$projname ElectrodeN1 TotalCurrent" y
cv_createDS $c2 "$projname time" "$projname ElectrodeN2 TotalCurrent" y
cv_createDS $c3 "$projname time" "$projname ElectrodeN3 TotalCurrent" y
cv_createDS $c4 "$projname time" "$projname BiasRail TotalCurrent" y

set Q1 [cv_compute "vecvaly(integr(<${c1}> - $LEAKAGE), 20e-9)" A A A A]
set Q2 [cv_compute "vecvaly(integr(<${c2}> - $LEAKAGE), 20e-9)" A A A A]
set Q3 [cv_compute "vecvaly(integr(<${c3}> - $LEAKAGE), 20e-9)" A A A A]
set Q4 [cv_compute "vecvaly(integr(<${c4}> - $LEAKAGE), 20e-9)" A A A A]

set N1 [expr {${Q1}/1.60217e-19}] 
set N2 [expr {${Q2}/1.60217e-19}] 
set N3 [expr {${Q3}/1.60217e-19}] 
set N4 [expr {${Q4}/1.60217e-19}] 

ft_scalar N1 [format %.0f $N1]
ft_scalar N2 [format %.0f $N2]
ft_scalar N3 [format %.0f $N3]
ft_scalar N4 [format %.0f $N4]
ft_scalar NTOT [format %.0f [expr {$N1+$N2+$N3}]]

script_exit
 




